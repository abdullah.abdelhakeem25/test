/****************************************************************************************************************************/
/*It�s required to write a main method that do the below																	*/
/*1- Ask the user to insert the username and password																			*/
/*2- If the username and password matches the values you set in the user class then system should print (Token) value.		*/
/*3- System should ask the user for a course to be registered in and takes the course name from the user						*/
/*4- if course name is available then thank you message will be printed to the user 										   */
/*****************************************************************************************************************************/

package projectClassDiagram;

import java.util.Scanner;

public class Application {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("");
		String username;
		String password;
		
		Scanner input = new Scanner(System.in);
		System.out.println("please enter username : ");
		username= input.nextLine();
		System.out.println("please enter Password : ");
		password= input.nextLine();
		
		Student S1=new Student();
		S1.setCredentials(username, password);
		
		if(S1.login())
		{
			System.out.println(S1.getToken());
			System.out.println("please enter course Name you want to be registered in:  ");//Java | C++ | Python
			String courseName =input.nextLine();
			if(S1.registerCourse(courseName))
			{
				System.out.println("Thank you for you registeration ");
			}
			else
			{
				System.out.println("Course not available ");
			}
		
		}
		else 
		{
			System.out.println("incorrect password or username >>> ");	
		}
		
	}

}
