package projectClassDiagram;

import java.util.Arrays;

public class Student extends User {

	private String studentNumber;
	private String registredCourse;
	private String[] availableCourse= {"Java","C++","Python"};
	
	
	public boolean registerCourse(String courseName)
	{
		if(checkCourseAvailability(courseName))
		{
			this.registredCourse=courseName;
			return true;
		}
		return false;
	}
	private boolean checkCourseAvailability(String courseName)
	{
		boolean contains = Arrays.stream(availableCourse).anyMatch(courseName::equals);
		return contains;
		
	}
	public void printRegisterdCourse() {
		System.out.println(this.registredCourse);
		
	}
	
}
